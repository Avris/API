<?php
namespace Avris\Api;

class Api
{
    public static $authenticationTypes = array('query', 'data', 'header');

    public static $decoders = array('text', 'json', 'xml');

    /** @var string */
    protected $host;

    /** @var array */
    protected $authentication = array();

    /** @var string */
    protected $certificate;

    /** @var string */
    protected $decoder;

    /**
     * @param $host
     * @param string $decoder
     * @throws \Exception
     */
    public function __construct($host, $decoder = 'text')
    {
        $this->host = $host;
        $this->setDecoder($decoder);
        $this->clearAuthentication();
    }

    /**
     * @param $path
     * @param array $query
     * @param string $method
     * @param array $data
     * @return mixed
     * @throws \Exception
     */
    public function call($path, $query = array(), $method = 'GET', $data = array())
    {
        $query = array_merge($this->authentication['query'], $query);

        $data = array_merge($this->authentication['data'], $data);

        $url = rtrim( $this->host . '/' . $path . '?' . http_build_query($query), '?');

        $response = $this->curl($url, strtoupper($method), $data);

        switch ($this->decoder) {
            case 'json':
                $decoded = json_decode($response, true);
                if (json_last_error()) {
                    throw new \Exception(sprintf('Cannot decode response. JSON error: %s', $decoded));
                }
                return $decoded;
            case 'xml':
                return new \SimpleXMLElement($response);
            case 'text':
            default:
                return $response;
        }
    }

    /**
     * @param $url
     * @param $method
     * @param $data
     * @return string
     * @throws \Exception
     */
    private function curl($url, $method, $data)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);

        switch ($method) {
            case 'GET': break;
            case 'POST':
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
                break;
            case 'PUT':
            case 'DELETE':
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
                break;
            default:
                throw new \Exception('Invalid request method');
        }

        curl_setopt($ch, CURLOPT_HTTPHEADER,
            array_map(function($key, $value) { return $key.': '. $value; },
            array_keys($this->authentication['header']), $this->authentication['header']));
				
        if ($this->certificate) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($ch, CURLOPT_CAINFO, $this->certificate);
        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        if ($output === false || curl_errno($ch)) {
            throw new \Exception(sprintf('Cannot connect to %s. cURL error: %s', $url, curl_error($ch)));
        }
        curl_close($ch);
        return $output;
    }

    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param string $host
     * @return $this
     */
    public function setHost($host)
    {
        $this->host = $host;
        return $this;
    }

    /**
     * @return string
     */
    public function getCertificate()
    {
        return $this->certificate;
    }

    /**
     * @param string $certificate
     * @return $this
     */
    public function setCertificate($certificate)
    {
        $this->certificate = $certificate;
        return $this;
    }

    /**
     * @return $this
     */
    public function clearCertificate()
    {
        $this->certificate = null;
    }

    /**
     * @param $type
     * @param $key
     * @param $value
     * @return $this
     * @throws \Exception
     */
    public function addAuthentication($type, $key, $value)
    {
        if (!in_array($type, self::$authenticationTypes)) {
            throw new \Exception('Incorrect authentication type. Correct types are: ' . implode(', ', self::$authenticationTypes));
        }

        $this->authentication[$type][$key] = $value;

        return $this;
    }

    /**
     * @return array
     */
    public function getAuthentication()
    {
        return $this->authentication;
    }

    /**
     * @return $this
     */
    public function clearAuthentication($type = null)
    {
        $types = $type ? array($type) : self::$authenticationTypes;

        foreach ($types as $type) {
            $this->authentication[$type] = array();
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getDecoder()
    {
        return $this->decoder;
    }

    /**
     * @param string $decoder
     * @return $this
     * @throws \Exception
     */
    public function setDecoder($decoder)
    {
        if (!in_array($decoder, self::$decoders)) {
            throw new \Exception('Incorrect decoder name. Correct names are: ' . implode(', ', self::$decoders));
        }

        $this->decoder = $decoder;
        return $this;
    }
}
