<pre>
    <?php
    include __DIR__ . '/../Api.php';
    use Avris\Api\Api;
    $api = new Api('http://localhost/AvrisApi/demo/api.php', 'json');

    $response = $api->call('products', array('minPrice' => 69, 'category' => 'manual'));
    var_dump($response);

    $api->addAuthentication('query', 'api_key', '5c2c0ccac98f15352421dc0d9ebf414f');
    $response = $api->call('products/13');
    var_dump($response);

    $api->addAuthentication('data', 'user', 'kowalski');
    $api->addAuthentication('data', 'password', 'dupa.8');
    $response = $api->call('products/13', array(), 'PUT', array('active' => 0));
    var_dump($response);

    $api->addAuthentication('header', 'X-Token', '3d898f5fa97812b530352c28835f9f6b215cfa75');
    $response = $api->call('products', array(), 'POST', array(
        'id' => 14,
        'name' => 'Test product',
        'categories' => array('foo', 'bar'),
        'price' => 123.45,
        'active' => 1,
    ));
    var_dump($response);

    $api->clearAuthentication()
        ->setDecoder('text');
    $response = $api->call('products/14', array(), 'DELETE');
    var_dump($response);

    $api->setDecoder('xml');
    $response = $api->call('', array('xml' => 'xml'));
    var_dump($response);
    ?>
</pre>