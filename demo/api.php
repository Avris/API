<?php

if (isset($_GET['xml'])) {
    echo '<?xml version="1.0"?><foo bar="baz"><lol/></foo>';die;
}

$headers = [];
foreach ($_SERVER as $key => $value) {
    if (strpos($key, 'HTTP_') === 0) {
        $headers[strtolower(str_replace('_', '-', substr($key, 5)))] = $value;
    }
}

header('content-type: application/json');
echo json_encode([
    'method' => $_SERVER['REQUEST_METHOD'],
    'url' => $_SERVER['PATH_INFO'],
    'query' => $_GET,
    'data' => $_POST,
    'headers' => $headers,
]);