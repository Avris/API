## Avris API ##

*Making REST API calls easier*

With this simple PHP class (which is a cURL wrapper) you can operate on REST API's as easily as possible!

Installation via Composer:

    composer require avris/api
    
Usage:

    $api = new Avris\Api\Api('https://api.example.com', 'json');
    $response = $api->call('user/77/products', array('maxPrice' => 100));
    $response = $api->call('user/77/products/123', array(), 'PUT', array('is_active' => 0));
    
First call will send a `GET` (default method) request to `https://api.example.com/user/77/products?maxPrice=100`.

Second call will send a `PUT` request to `https://api.example.com/user/77/products/123` with data `is_active => 0`.

Trivial, isn't it?

Every response is also automatically JSON decoded, because we specified it in the second argument of the constructor. Accepted decoders are: `text` (plaintext, default), `json` and `xml`.

By using `$api->addAuthentication`, you can order the library to append some set of data to **every** request. Accepted types are: `query` ($\_GET), `data` ($\_POST) and `header` (HTTP headers). For example:

    $api->addAuthentication('header', 'X-Token', '3d898f5fa97812b530352c28835f9f6b215cfa75');
    
Will append a `X-Token: 3d898f5fa97812b530352c28835f9f6b215cfa75` header to every request made.

To connect to a server via HTTPS using a certificate, just invoke this function:
 
    $api->setCertificate(__DIR__ . '/server.crt');
    
_Logo credits to: [Aha-Soft](https://thenounproject.com/ahasoft/)_